#include "drone_one_image_loggerrosmodule.h"

bool DroneLoggerROSModule::save_front_camera_images  = false;
bool DroneLoggerROSModule::save_bottom_camera_images = false;
bool DroneLoggerROSModule::save_right_camera_images  = false;
bool DroneLoggerROSModule::save_left_camera_images   = false;
bool DroneLoggerROSModule::save_back_camera_images   = false;

DroneLoggerROSModule::DroneLoggerROSModule( ros::NodeHandle n) :
    DroneLoggerROSBasicModule(n),
    //Cameras
    front_camera_subscriber(n,"camera;front", DRONE_LOGGER_SENSOR_FRONT_CAMERA, &DroneLoggerROSModule::droneFrontCameraCallback, init_rostime, 0, 3),
    bottom_camera_subscriber(n,"camera;bottom", DRONE_LOGGER_SENSOR_BOTTOM_CAMERA, &DroneLoggerROSModule::droneBottomCameraCallback, init_rostime, 0, 3),
    right_camera_subscriber(n,"camera;right", DRONE_LOGGER_SENSOR_RIGHT_CAMERA, &DroneLoggerROSModule::droneRightCameraCallback, init_rostime, 0, 3),
//    left_camera_subscriber(n,"camera;left", DRONE_LOGGER_SENSOR_LEFT_CAMERA, &DroneLoggerROSModule::droneLeftCameraCallback, init_rostime, 0, 3),
//    back_camera_subscriber(n,"camera;back", DRONE_LOGGER_SENSOR_BACK_CAMERA, &DroneLoggerROSModule::droneBackCameraCallback, init_rostime, 0, 3),
    // This boolean is added to perform the reading of configuration files outside of the constructors
    configuration_files_are_read(false)
{
    return;
}

DroneLoggerROSModule::~DroneLoggerROSModule()
{
    return;
}

//////// IMAGES
std::string DroneLoggerROSModule::droneFrontCameraCallback(const sensor_msgs::ImageConstPtr &msg)
{
    std::stringstream return_ss;
    std::stringstream image_ss;
    static int numImage=-1;

    numImage++;

    std::string pathToSave=DroneLoggerROSBasicModule::currentlog_path+"/frontImage/";
    image_ss  << "im_" << std::setfill('0') << std::setw(10) << numImage << ".png";
    return_ss << "image:" << image_ss.str();

    if (DroneLoggerROSModule::save_front_camera_images) {
        //Front image
        cv_bridge::CvImagePtr cvFrontImage;
        cv::Mat frontImage;

        try
        {
            cvFrontImage = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return return_ss.str();
        }

        frontImage=cvFrontImage->image;


        //Store in memory
        cv::imwrite( pathToSave+image_ss.str(), frontImage );
    }

    return return_ss.str();
}

std::string DroneLoggerROSModule::droneBottomCameraCallback(const sensor_msgs::ImageConstPtr &msg)
{
    std::stringstream return_ss;
    std::stringstream image_ss;
    static int numImage=-1;
    numImage++;

    std::string pathToSave=DroneLoggerROSBasicModule::currentlog_path+"/bottomImage/";
    image_ss  << "im_" << std::setfill('0') << std::setw(10) << numImage << ".png";
    return_ss << "image:" << image_ss.str();

    if (DroneLoggerROSModule::save_bottom_camera_images) {
        //Bottom image
        cv_bridge::CvImagePtr cvBottomImage;
        cv::Mat BottomImage;

        try
        {
            cvBottomImage = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return return_ss.str();
        }

        BottomImage=cvBottomImage->image;


        //Store in memory
        cv::imwrite( pathToSave+image_ss.str(), BottomImage );
    }

    return return_ss.str();
}

std::string DroneLoggerROSModule::droneRightCameraCallback(const sensor_msgs::ImageConstPtr &msg)
{
    std::stringstream return_ss;
    std::stringstream image_ss;
    static int numImage=-1;
    numImage++;

    std::string pathToSave=DroneLoggerROSBasicModule::currentlog_path+"/rightImage/";
    image_ss  << "im_" << std::setfill('0') << std::setw(10) << numImage << ".png";
    return_ss << "image:" << image_ss.str();

    if (DroneLoggerROSModule::save_right_camera_images) {
        //Right image
        cv_bridge::CvImagePtr cvRightImage;
        cv::Mat RightImage;

        try
        {
            cvRightImage = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return return_ss.str();
        }

        RightImage=cvRightImage->image;


        //Store in memory
        cv::imwrite( pathToSave+image_ss.str(), RightImage );
    }

    return return_ss.str();
}

std::string DroneLoggerROSModule::droneLeftCameraCallback(const sensor_msgs::ImageConstPtr &msg)
{
    std::stringstream return_ss;
    std::stringstream image_ss;
    static int numImage=-1;
    numImage++;

    std::string pathToSave=DroneLoggerROSBasicModule::currentlog_path+"/leftImage/";
    image_ss  << "im_" << std::setfill('0') << std::setw(10) << numImage << ".png";
    return_ss << "image:" << image_ss.str();

    if (DroneLoggerROSModule::save_left_camera_images) {
        //Left image
        cv_bridge::CvImagePtr cvLeftImage;
        cv::Mat LeftImage;

        try
        {
            cvLeftImage = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return return_ss.str();
        }

        LeftImage=cvLeftImage->image;


        //Store in memory
        cv::imwrite( pathToSave+image_ss.str(), LeftImage );
    }

    return return_ss.str();
}

std::string DroneLoggerROSModule::droneBackCameraCallback(const sensor_msgs::ImageConstPtr &msg)
{
    std::stringstream return_ss;
    std::stringstream image_ss;
    static int numImage=-1;
    numImage++;

    std::string pathToSave=DroneLoggerROSBasicModule::currentlog_path+"/backImage/";
    image_ss  << "im_" << std::setfill('0') << std::setw(10) << numImage << ".png";
    return_ss << "image:" << image_ss.str();

    if (DroneLoggerROSModule::save_back_camera_images) {
        //Back image
        cv_bridge::CvImagePtr cvBackImage;
        cv::Mat BackImage;

        try
        {
            cvBackImage = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return return_ss.str();
        }

        BackImage=cvBackImage->image;


        //Store in memory
        cv::imwrite( pathToSave+image_ss.str(), BackImage );
    }

    return return_ss.str();
}

bool DroneLoggerROSModule::read_configuration_files()
{
    try {
        XMLFileReader my_xml_reader(stackPath+"configs/drone"+ cvg_int_to_string(idDrone)+"/drone_logger_config.xml");
        DroneLoggerROSModule::save_front_camera_images  = (bool) my_xml_reader.readIntValue( "drone_logger_config:save_front_camera_images" );
        DroneLoggerROSModule::save_bottom_camera_images = (bool) my_xml_reader.readIntValue( "drone_logger_config:save_bottom_camera_images" );
        DroneLoggerROSModule::save_right_camera_images  = (bool) my_xml_reader.readIntValue( "drone_logger_config:save_right_camera_images" );
        DroneLoggerROSModule::save_left_camera_images = (bool) my_xml_reader.readIntValue( "drone_logger_config:save_left_camera_images" );
        DroneLoggerROSModule::save_back_camera_images  = (bool) my_xml_reader.readIntValue( "drone_logger_config:save_back_camera_images" );
    }
    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
        DroneLoggerROSModule::save_front_camera_images  = false;
        DroneLoggerROSModule::save_bottom_camera_images = false;
        DroneLoggerROSModule::save_right_camera_images  = false;
        DroneLoggerROSModule::save_left_camera_images = false;
        DroneLoggerROSModule::save_back_camera_images  = false;
    }

    return true;
}

bool DroneLoggerROSModule::run() {
    if (!configuration_files_are_read) {
        read_configuration_files();
        configuration_files_are_read = true;
    }

    if(!DroneLoggerROSBasicModule::run()) {
        return false;
    }

    return true;
}
